# Team provide the association between Player and Coach
# when a coaches make a team of payer, an instance of Team os created
# showing that a coach has player

class Team:
    def __init__(self, team_name, stats_team, stats_players):
        self.team_name = team_name
        self.list_player = []
        self.list_coach = []
        self.stats_team = stats_team
        self.stats_players = stats_players

    def get_list_player(self):
        return self.list_player

    def get_list_coach(self):
        return self.list_coach

    def get_team_name(self):
        return self.team_name

    def add_player(self, p):
        self.list_player.append(p)

    def add_coach(self, c):
        self.list_coach.append(c)

    def add_stats_players(self, p):
        self.stats_players = self.stats_players + p.get_stats()

    def remove_player(self, p):
        for p1 in self.list_player:
            if p1 == p:
                self.list_player.remove(p)

    def remove_coach(self, c):
        for c1 in self.list_coach:
            if c1 == c:
                self.list_coach.remove(c)

    def substract_stats_players(self, p):
        self.stats_players = self.stats_players - p.get_stats()

    def to_string(self):
        c_string = ""
        p_string = ""
        for c in self.list_coach:
            c_string = c_string + c.to_string() +"\n"

        for p in self.list_player:
            p_string = p_string + p.to_string() +"\n"

        b = self.stats_team
        s = self.team_name + "\nRecord: " + str(b[0]) + " wins " + str(b[1]) + " lost " + str(b[2]) + " ties\nOverall stats of all player combine: gp g a p s p/m\n                                      " + self.stats_players.to_string() + "\nCoaches:\n" + c_string + "Players:\n"+ p_string
        return s

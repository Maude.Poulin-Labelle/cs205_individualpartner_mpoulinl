import unittest
import league
import coach
import player
import stats
import team

# unit tests for my league application
#
# Define a class that is subclassed from unittest.Testcase
#
# There are four functions you can implement
#
# (1) static member function setUpClass()
# - this will be called one time, before any tests run
# - the cls parameter will be an instance of this class:
#   it's how you can refer to member variables
# - this is a useful one--you can set things up here for your tests
#
# (2) static member function tearDownClass()
# - do any cleanup you need (you might not need to do any; so in this case
#   you don't have to implement the function
#
# (3) setUp()
# - called before each test
# - if you don't need to do anything before each test, then don't implement this
#
# (4) tearDown()
# - called after each test
# - I use this to clean up after each test
# - but again, if you don't need to do any cleanup after each tests,
#   then don't implement this
#
# in PyCharm, you can run your tests by right-clicking on your test file
# and then doing "Run Unittests in [your filename]
#
# View -> Tool Windows -> Run will show you the results of your tests

class TestCheckout(unittest.TestCase):
    league = None

    @classmethod
    def setUpClass(cls):
        # called one time, at the very beginning
        print('setUpClass()')
        cls.league = league.League().get()

        # create a few players and patrons
        cls.stat1_noTeam = stats.Stats(5, 3, 4, 2, 4)
        cls.stat1_UVM = stats.Stats(5, 3, 4, 2, 4)
        cls.stat2_UVM = stats.Stats(5, 4, 5, 7, 3)
        cls.stat1_BC = stats.Stats(5, 1, 7, 2, 5)
        cls.stat2_BC = stats.Stats(5, 3, 2, 10, -2)

        cls.compare_stat1 = stats.Stats(1, 0, 0, 0, 0)
        cls.compare_stat2 = stats.Stats(1, 2, 2, 2, 2)
        cls.test_stat2 = stats.Stats(1, 1, 1, 1, 1)
        cls.test_stat3 = stats.Stats(1, 1, 1, 1, 1)

        # Create Players
        cls.player1_no_team = player.Player(2, "No team player", 'D', "Jr", "NoTeam", cls.stat1_noTeam)
        cls.player1_UVM = player.Player(2, "Sini Karjalainen", 'D', "Jr", "UVM", cls.stat1_UVM)
        cls.player2_UVM = player.Player(3, "Bella Parento", 'D', "Fr", "UVM", cls.stat2_UVM)
        cls.player1_BC = player.Player(18, "Alexie Guay", 'D', "Sr", "BC", cls.stat1_BC)
        cls.player2_BC = player.Player(4, "Cayla Barns", 'D', "Fr", "BC", cls.stat2_BC)

        #Create coaches
        cls.coach1_no_team = coach.Coach("Jim Plumer", "Head coach", "No Team")
        cls.coach1_UVM = coach.Coach("Jim Plumer", "Head coach", "UVM")
        cls.coach2_UVM = coach.Coach("Jessica Koizumi", "Assistant coach", "UVM")
        cls.coach1_BC = coach.Coach("Mark s", "Head coach", "BC")
        cls.coach2_BC = coach.Coach("Lisa b", "Assistant coach", "BC")

        cls.team_UVM = team.Team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        cls.team_BC = team.Team("BC", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        # we'll use the players and the patrons in the tests, so make them class variables
        cls.league.add_team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        cls.league.add_team("BC", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))


    @classmethod
    def tearDownClass(cls):
        # called one time, at the very end--if you need to do any final cleanup, do it here
        print('tearDownClass()')

    def setUp(self):
        # called before every test
        print('setUp()')

    def tearDown(self):
        # called after every test
        print('tearDown()')

    # -------------------------------------------------------------

    def test_player(self):
        self.league.remove_player(self.player2_UVM)
        # check that the a player cannot be added if team not created
        add_player_to_unknown_team = self.league.add_player(self.player1_no_team)
        self.assertEqual(add_player_to_unknown_team,0) #since team need to be created first

        # check that the a can player be added when team is created
        self.league.add_team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        add_player_to_uvm = self.league.add_player(self.player1_UVM)
        self.assertEqual(add_player_to_uvm, 1)

        #check if that only the player with an availble team was added in the list of players
        players = self.league.get_players()
        self.assertEqual(len(players), 1)

        #find a player by name that does not exist
        players = self.league.find_player_by_name("Sini")
        self.assertEqual(len(players), 0)

        #look for a player that exist
        players = self.league.find_player_by_name("Sini Karjalainen")
        self.assertEqual(len(players), 1)

        #remove player and check if it was removed
        is_player_removed = self.league.remove_player(self.player1_UVM)
        self.assertEqual(is_player_removed, 1)
        players = self.league.get_players()
        self.assertEqual(len(players), 0)

    def test_coach(self):
        self.tearDown()
        # check that the a coach cannot be added if team not created
        add_coach_to_unknown_team = self.league.add_coach(self.coach1_no_team)
        self.assertEqual(add_coach_to_unknown_team,0) #since team need to be created first

        # check that the a can coach be added when team is created
        self.league.add_team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        add_coach_to_uvm = self.league.add_coach(self.coach1_UVM)
        self.assertEqual(add_coach_to_uvm, 1)

        #check if that only the coach with an availble team was added in the list of players
        c = self.league.get_coaches()
        self.assertEqual(len(c), 1)

        #find a coach by name that does not exist
        c = self.league.find_coach_by_name("Jim")
        self.assertEqual(len(c), 0)

        #look for a coach that exist
        c = self.league.find_coach_by_name("Jim Plumer")
        self.assertEqual(len(c), 1)

        #remove coach and check if it was removed
        is_coach_removed = self.league.remove_coach(self.coach1_UVM)
        self.assertEqual(is_coach_removed, 1)
        c = self.league.get_coaches()
        self.assertEqual(len(c), 0)

    def test_stats(self):
        # Test if adding two stat object work
        s1 = self.test_stat2 + self.test_stat3
        is_equal1 = (s1 == self.compare_stat2)
        self.assertEqual(is_equal1, True)

        # Test if subtracting two stats object work
        s = self.test_stat2 - self.test_stat3
        is_equal = s == self.compare_stat1
        self.assertEqual(is_equal, True)


    def test_adding_correct(self):
        self.league.add_team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
        add_player = self.league.add_player(self.player2_UVM)
        self.assertEqual(add_player, 1)
        add_player = self.league.add_player(self.player2_UVM)
        self.assertEqual(add_player, 0)
        self.league.remove_player(self.player2_UVM)

        add_coach = self.league.add_coach(self.coach2_UVM)
        self.assertEqual(add_coach, 1)
        add_coach = self.league.add_coach(self.coach2_UVM)
        self.assertEqual(add_coach, 0)
        self.league.remove_coach(self.coach2_UVM)


    def test_adding_incorrect(self):
        add_player = self.league.add_player_incorrect(self.player2_UVM)
        self.assertEqual(add_player, 1)
        add_player_same = self.league.add_player_incorrect(self.player2_UVM)
        self.assertEqual(add_player_same, 0)
        self.league.remove_player(self.player2_UVM)
        self.league.remove_player(self.player2_UVM)

        add_coach = self.league.add_coach_incorrect(self.coach2_UVM)
        self.assertEqual(add_coach, 1)
        add_coach = self.league.add_coach_incorrect(self.coach2_UVM)
        self.assertEqual(add_coach, 0)
        self.league.remove_coach(self.coach2_UVM)
        self.league.remove_coach(self.coach2_UVM)


if __name__ == "__main__":
    unittest.main()

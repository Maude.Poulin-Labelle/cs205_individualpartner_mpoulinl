# Player: The League has instance of Player

class Player:
    def __init__(self,number,name, position,year,team_name,stats):
        self.number = number
        self.name = name
        self.position = position
        self.year = year
        self.team_name = team_name
        self.stats = stats

    #need to be fixed
    def to_string(self):
        s = ' ' +self.stats.to_string() + ' ' + self.team_name + ' ' + self.name + ' ' + self.year + '  ' +self.position
        return s

    #getters
    def get_stats(self):
        return self.stats

    def get_player(self):
        return self

    def get_year(self):
        return self.year

    def get_position(self):
        return self.position

    def get_name(self):
        return self.name

    def get_team_name(self):
        return self.team_name
    # Override methods



class Stats:
    def __init__(self, game_play, goal, assist,shot, plus_minus):
        self.game_play = game_play
        self.goal = goal
        self.assist = assist
        self.points = goal + assist
        self.shot = shot
        self.plus_minus = plus_minus

    def get_game_play(self):
        return self.game_play

    def get_goal(self):
        return self.goal

    def get_assist(self):
        return self.assist

    def get_points(self):
        return self.points

    def get_shot(self):
        return self.shot

    def get_plus_minus(self):
        return self.plus_minus

    def to_string(self):
        s = str(self.game_play) + " " + str(self.goal) + " " + str(self.assist) + " " + str(self.points) + " " + str(self.shot) + " " + str(self.plus_minus)
        return s

    def __add__(self, other):
        new_stats_object = Stats(0,0,0,0,0)

        if self.game_play == 0:
            new_stats_object.game_play = other.game_play
        else:
            new_stats_object.game_play = self.game_play

        new_stats_object.goal = self.goal + other.goal
        new_stats_object.assist = self.assist + other.assist
        new_stats_object.points= self.points + other.points
        new_stats_object.shot = self.shot + other.shot
        new_stats_object.plus_minus = self.plus_minus + other.plus_minus
        return new_stats_object

    def __sub__(self, other):
        new_stats_object = Stats(0,0,0,0,0)

        if self.game_play > other.game_play:
            new_stats_object.game_play = self.game_play
        else:
            new_stats_object.game_play = other.game_play

        new_stats_object.goal = self.goal - other.goal
        new_stats_object.assist = self.assist - other.assist
        new_stats_object.points= self.points - other.points
        new_stats_object.shot = self.shot - other.shot
        new_stats_object.plus_minus = self.plus_minus - other.plus_minus
        return new_stats_object


    def __eq__(self, other):
        if self.game_play == other.get_game_play() and self.shot == other.get_shot() and self.assist == other.get_assist() and self.plus_minus == other.get_plus_minus():
            return True

        return False

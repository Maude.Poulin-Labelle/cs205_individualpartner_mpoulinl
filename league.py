import team

class League:
    l = None

    @classmethod
    def get(self):
        if self.l is None:
            self.l = League()
        return self.l

    def __init__(self):
        self.players = set()
        self.coaches = set()
        self.teams = set()

    #Adding
    def add_team(self, team_name, stats_team, stats_players):
        t = team.Team(team_name, stats_team, stats_players)
        self.teams.add(t)

    def add_coach(self, coach):
        for t in self.teams:
            #look if the team added for the coach exist
            if t.get_team_name() == coach.get_team_name():
                for c in self.coaches:
                    # look if it already exist
                    if c.get_name() == coach.get_name() and c.get_status() == coach.get_status():
                        return 0 #twice the same coacgh
                self.coaches.add(coach)
                t.add_coach(coach)
                return 1
        #Team not existant/Coach not added
        return 0 #coach not added

    def add_player(self, player):
        for t in self.teams:
            if t.get_team_name() == player.get_team_name():
                for p in self.players:
                    if p.get_name() == player.get_name() and p.get_year() == player.get_year() and p.get_position() == player.get_position():
                        return 0
                self.players.add(player)
                t.add_player(player)
                t.add_stats_players(player)
                return 1
        #Team not existant/Player not added
        return 0

    def add_coach_incorrect(self,coach):
        # add a coach without looking if it already exist
        # would had twice the same coach which is wrong
        self.coaches.add(coach)
        for t in self.teams:
            if t.get_team_name() == coach.get_team_name():
                t.add_coach(coach)
                return 1 #coach added
        # Team not existant/Coach not added
        return 0 #coach not added

    def add_player_incorrect(self, player):
        # add a player without looking if it already exist
        for t in self.teams:
            if t.get_team_name() == player.get_team_name():
                self.players.add(player)
                t.add_player(player)
                t.add_stats_players(player)
                return 1 # player added
        # Team not existant/Player not added
        return 0    # player not added

    #Getting
    def get_teams(self):
        return self.teams

    def get_coaches(self):
        return self.coaches

    def get_coach_name_team_name(self, name, team_name):
        for t in self.teams:
            if t.get_team_name() == team_name:
                for c in self.coaches:
                    if c.get_name() == name:
                        print("Coach found/get :" + c.to_string())
                        return c
        print("coach not found\get")

    def get_players(self):
        return self.players

    def get_player_name_team_name(self, name, team_name):
        return self.coaches

    def get_number_player(self):
        return len(self.players)

    def get_number_coaches(self):
        return len(self.coaches)

    #showing/printing
    def show_teams(self):
        for t in self.teams:
            print(t.to_string())

    def show_player_position(self, pos):
        player = []
        for p in self.players:
            if p.get_position == pos:
                player.append(p)
        if len(player) == 0:
            print("No player found at this position")
        else:
            for p in player:
                print(p.to_string())

    def show_player_year(self, y):
        player = []
        for p in self.players:
            if p.get_year() == y:
                player.append(p)
        if len(player) == 0:
            print("No player found at this position")
        else:
            for p in player:
                print(p.to_string())

    def show_roster(self, team_name):
        for t in self.teams:
            if t.get_team_name() == team_name:
                print(t.to_string())
                return 1
        print("The team does not exist")
        return 0

    def show_stats_leader(self, num):
        explain_stats = ""
        players = []
        for p in self.players:
            if len(players) == 0:
                players.append(p)
            else:
                index = 0
                size = len(players)
                while (size == len(players)):
                    #Game Played
                    if num == 0:
                        explain_stats = "Leader for gp"
                        if players[index].get_stats().get_game_play() < p.get_stats().get_game_play():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    #Goals
                    elif num == 1:
                        explain_stats = "Leader for g"
                        if players[index].get_stats().get_goal() < p.get_stats().get_goal():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    #Assist
                    elif num == 2:
                        explain_stats = "Leader for a"
                        if players[index].get_stats().get_assist() < p.get_stats().get_assist():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    #Points
                    elif num == 3:
                        explain_stats = "Leader for p"
                        if players[index].get_stats().get_points() < p.get_stats().get_points():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    #Shots
                    elif num == 4:
                        explain_stats = "Leader for s"
                        if players[index].get_stats().get_shot() < p.get_stats().get_shot():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    #plus Minus
                    elif num == 5:
                        explain_stats = "Leader for p/m"
                        if players[index].get_stats().get_plus_minus() < p.get_stats().get_plus_minus():
                            players.insert(index, p)
                        if index == (size - 1):
                            if size == len(players):
                                players.append(p)
                    else:
                        num = 6

                    index = index + 1
        if num == 6:
            print("Sorry wrong value inserted: 0:gp, 1:g, 2:a, 3:p, 4:s, 5:p/m")
            #empty
            return players
        elif len(players) == 0:
            print("No player in the league")
            #empty
            return players
        else:
            print(explain_stats)
            print("gp g a p s p/m")
            for pstats in players:
                print(pstats.to_string())
            return players

    #Finding
    def find_coach_by_name(self, name):
        c1 = []
        for c in self.coaches:
            if c.get_name() == name:
                c1.append(c)
        return c1

    def find_player_by_name(self, name):
        players = []
        for p in self.players:
            if p.get_name() == name:
                players.append(p)
        #will be empty if none
        return players


    #remove
    def remove_coach(self, coach):
        for c in self.coaches:
            if c == coach:
                for t in self.teams:
                    if t.get_team_name() == c.get_team_name():
                        t.remove_coach(coach)
                self.coaches.remove(coach)
                return 1
        return 0

    def remove_player(self, p2):
        for p in self.players:
            if p == p2:
                for t in self.teams:
                    if t.get_team_name() == p.get_team_name():
                        t.remove_player(p2)
                        t.substract_stats_players(p2)

                self.players.remove(p2)
                return 1
        return 0
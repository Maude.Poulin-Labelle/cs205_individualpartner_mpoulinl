# Coach: there will be 3 coaches per team

class Coach:
    def __init__(self, name, status, team_name):
        self.name = name
        self.status = status
        self.team_name = team_name

    def to_string(self):
        s = self.name + ' ' + self.status
        return s

    # getters
    def get_name(self):
        return self.name

    def get_team_name(self):
        return self.team_name

    def get_status(self):
        return self.status

    def get_coach(self):
        return self

    def add_player_roster(self, player):
        self.player_roster.add(player)

    def get_player_roster(self):
        return list(self.player_roster)



import player
import stats
import coach
import league

def simple_runtime_test(hockey_east):
    print("Creating two teams : BC and UVM")
    hockey_east.add_team("UVM", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
    hockey_east.add_team("BC", [0, 0, 0], stats.Stats(0, 0, 0, 0, 0))
    hockey_east.show_teams()

    print("Adding two UVM Players")
    # Create Stats
    stat1_UVM = stats.Stats(5, 3, 4, 2, 4, )
    stat2_UVM = stats.Stats(5, 4, 5, 7, 3, )
    # Create Players
    player3_UVM = player.Player(2, "Sini Karjalainen", 'D', "Jr", "no_team", stat1_UVM)
    player1_UVM = player.Player(2, "Sini Karjalainen", 'D', "Jr", "UVM", stat1_UVM)
    player2_UVM = player.Player(3, "Bella Parento", 'D', "Fr", "UVM", stat2_UVM)

    print(hockey_east.add_player(player3_UVM))
    hockey_east.add_player(player1_UVM)
    hockey_east.add_player(player2_UVM)
    hockey_east.show_roster("UVM")
    print("Removing the player and showing the roster again, we can see stats went to zero bu not gp (since techincally removing player would not reduce number of game played by a team")
    hockey_east.remove_player(player1_UVM)
    hockey_east.remove_player(player2_UVM)
    hockey_east.show_roster("UVM")

    print("Adding four coaches: two UVM, two BC")
    coach1_UVM = coach.Coach("Jim Plumer", "Head coach", "UVM")
    coach2_UVM = coach.Coach("Jessica Koizumi", "Assistant coach", "UVM")
    coach1_BC = coach.Coach("Mark s", "Head coach", "BC")
    coach2_BC = coach.Coach("Lisa b", "Assistant coach", "BC")
    hockey_east.add_coach(coach1_UVM)
    hockey_east.add_coach(coach2_UVM)
    hockey_east.add_coach(coach1_BC)
    hockey_east.add_coach(coach2_BC)

    hockey_east.show_teams()

    print("Adding Two Bc player and re-add two UVM player and looking at stats of p and plus minus")
    stat1_BC = stats.Stats(5, 1, 7, 2, 5)
    stat2_BC = stats.Stats(5, 3, 2, 10, -2)
    player1_BC = player.Player(18, "Alexie Guay", 'D', "Sr", "BC", stat1_BC)
    player2_BC = player.Player(4, "Cayla Barns", 'D', "Fr", "BC", stat2_BC)
    hockey_east.add_player(player1_UVM)
    hockey_east.add_player(player2_UVM)
    hockey_east.add_player(player1_BC)
    hockey_east.add_player(player2_BC)

    hockey_east.show_stats_leader(1)
    hockey_east.show_stats_leader(5)

    print("show player of year Fr")
    hockey_east.show_player_year("Fr")

    print("get coach Jim Plumer UVM")
    c = hockey_east.get_coach_name_team_name("Jim Plumer", "UVM")
    print("Remove it and print the whole team again")
    hockey_east.remove_coach(c)
    hockey_east.show_teams()


    # END TEAM UVM---------------------------------------------------------------------------------



def main():
    hockey_east = league.League().get()

    simple_runtime_test(hockey_east)

main()

